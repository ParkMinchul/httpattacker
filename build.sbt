name := "HttpAttacker"

version := "1.0"

scalaVersion := "2.11.7"


libraryDependencies ++= {
  val AkkaV = "2.4.1"
  Seq(
    "com.typesafe.akka" %% "akka-actor" % AkkaV,
    "com.typesafe.akka" %% "akka-http-experimental" % "2.0-M2"
  )
}