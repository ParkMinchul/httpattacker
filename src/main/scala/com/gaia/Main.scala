package com.gaia

import akka.actor.{Props, ActorSystem}
import akka.http.scaladsl.Http
import scala.concurrent.duration._
import com.typesafe.config.ConfigFactory

/**
 * Created by ktz on 15. 12. 15.
 */
object Main extends App{
  implicit val system = ActorSystem()
  val configFactory = ConfigFactory.load("application")

  val http = Http(system)
  (0 until configFactory.getInt("httpClient.nOfClient")).toList.par.foreach{_=>
    system.actorOf(Props(new HttpClient(s"${configFactory.getString("httpClient.siteURL")}:${configFactory.getInt("httpClient.sitePort")}${configFactory.getString("httpClient.siteURI")}",
      configFactory.getBoolean("httpClient.showHTML"), configFactory.getLong("httpClient.nOfInterval") millisecond, http)))
  }
}
