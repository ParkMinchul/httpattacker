package com.gaia

import scala.concurrent.duration._
import akka.actor.{ActorLogging, Actor}
import akka.http.scaladsl.HttpExt
import akka.http.scaladsl.model._
import akka.stream.scaladsl.ImplicitMaterializer
import akka.util.ByteString
import scala.util.{Failure, Success}


/**
 * Created by ktz on 15. 12. 15.
 */
class HttpClient(val uri : String, val ShowHTML : Boolean, val interval : FiniteDuration, val http : HttpExt) extends Actor with ImplicitMaterializer with ActorLogging{
  import akka.pattern.pipe
  import context.dispatcher

  context.system.scheduler.schedule(0 millisecond, interval)(http.singleRequest(HttpRequest(uri = this.uri)).pipeTo(self))

  def receive = {
    case HttpResponse(StatusCodes.OK, headers, entity, _)=>
      entity.dataBytes.runFold(ByteString(""))(_ ++ _).onComplete{
        case Success(byteString) =>
          if(ShowHTML)log.info(s"Got response, body: \n${byteString.utf8String}") else log.info("Got response")
        case Failure(e) =>
          log.error("Cannot Parse byteString - It`s not utf-8")
      }
    case HttpResponse(code, _, _, _) =>
      log.info("Request failed, response code: " + code)
  }
}